/*
 * Copyright (C) 2016 Eli
 * 
 * This file is part of L2Next Project. (https://bitbucket.org/l2next/)
 * 
 * You are not allowed to use this class outside of L2Next Project. 
 * This includes but is not limited to distributing, modifying or use of binary 
 * or source code, complete or partial, without written permission by author.
 *
 */
package com.l2jserver.gameserver.custom.Helpers;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.L2GameClient;

/**
 * @author Eli
 */
public class IpUtils
{
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Base Methods.
	//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * @return Stream<L2PcInstance> of L2World.getInstance().getPlayers()
	 */
	public static Stream<L2PcInstance> getCharactersStream()
	{
		return L2World.getInstance().getPlayers().stream();
	}
	
	/**
	 * Returns a List<L2PcInstance> matching filter parameter and with an active non-detached client.
	 * @param filter Predicate for further filtering resulting output.
	 * @return List<L2PcInstance> matching filter parameter and with an active non-detached client.
	 */
	public static List<L2PcInstance> filterOnlineCharacters(Predicate<L2PcInstance> filter)
	{
		return getCharactersStream().filter(filter).filter(e -> (e.getClient() != null) && !e.getClient().isDetached()).collect(Collectors.toList());
	}
	
	/**
	 * Returns a List<L2PcInstance> with an active non-detached client.
	 * @return List<L2PcInstance> with an active non-detached client.
	 */
	public static List<L2PcInstance> filterOnlineCharacters()
	{
		return getCharactersStream().filter(e -> (e.getClient() != null) && !e.getClient().isDetached()).collect(Collectors.toList());
	}
	
	/**
	 * Returns a List<L2PcInstance> matching filter parameter and with an missing or detached client.
	 * @param filter Predicate for further filtering resulting output.
	 * @return List<L2PcInstance> matching filter parameter and with an missing or detached client.
	 */
	public static List<L2PcInstance> filterDetachedCharacters(Predicate<L2PcInstance> filter)
	{
		return getCharactersStream().filter(filter).filter(e -> (e.getClient() == null) || e.getClient().isDetached()).collect(Collectors.toList());
	}
	
	/**
	 * Returns a List<L2PcInstance> with an missing or detached client.
	 * @return List<L2PcInstance> with an missing or detached client.
	 */
	public static List<L2PcInstance> filterDetachedCharacters()
	{
		return getCharactersStream().filter(e -> (e.getClient() == null) || e.getClient().isDetached()).collect(Collectors.toList());
	}
	
	/**
	 * @param ipAddress String of ipAddress
	 * @param player L2PcInstance which ipAddress will be compared.
	 * @return TRUE if match. FALSE if no match OR one of parameter=null OR no client.
	 */
	public static boolean compareIp(String ipAddress, L2PcInstance player)
	{
		if ((ipAddress == null) || (player == null) || (player.getClient() == null) || !ipAddress.equals(player.getClient().getConnection().getInetAddress().getHostAddress()))
		{
			return false;
		}
		return true;
	}
	
	/**
	 * Returns the local ip address of a L2PcInstance as a String.
	 * @param player Local Ip of this L2PcInstance.
	 * @return Ip formatted as String.
	 */
	public static String getPlayerTracertString(L2PcInstance player)
	{
		if ((player.getClient() == null) || player.getClient().isDetached())
		{
			return "0.0.0.0";
		}
		
		String pcIp = "";
		int[][] trace = player.getClient().getTrace();
		for (int i = 0; i < trace[0].length; i++)
		{
			pcIp = (!pcIp.isEmpty() ? "." : "") + pcIp + trace[0][i];
		}
		
		return pcIp;
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// getByIp Methods for PLAYERS and GMS.
	//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Returns a list of L2PcInstances which are connected from the same IP as the given L2PcInstance. (Excluding detached clients)
	 * @param character of which IP is matched against all L2PcInstances.
	 * @return List<L2PcInstance> with matching players or EMPTY list. Never null.
	 * @author Eli
	 */
	public static List<L2PcInstance> getByIp(L2PcInstance character)
	{
		L2GameClient client = Objects.requireNonNull(character).getClient();
		if ((client == null))
		{
			return new ArrayList<>();
		}
		return getByIp(client);
	}
	
	/**
	 * Returns a list of L2PcInstances which are connected from the same IP as the given L2GameClient. (Excluding detached clients)
	 * @param client of which IP is matched against all L2PcInstances.
	 * @return List<L2PcInstance> with matching players or EMPTY list. NEVER NULL.
	 * @author Eli
	 */
	public static List<L2PcInstance> getByIp(L2GameClient client)
	{
		String ip = Objects.requireNonNull(client).getConnectionAddress().getHostAddress();
		return getByIp(ip);
	}
	
	/**
	 * Returns a list of L2PcInstances which are connected from the same IP as the given InetAddress. (Excluding detached clients)
	 * @param ipAddress which is matched against all L2PcInstances.
	 * @return List<L2PcInstance> with matching players or EMPTY list. NEVER NULL.
	 * @author Eli
	 */
	public static List<L2PcInstance> getByIp(InetAddress ipAddress)
	{
		String ip = Objects.requireNonNull(ipAddress).getHostAddress();
		return getByIp(ip);
	}
	
	/**
	 * Returns a list of L2PcInstances which are connected from the same IP as the given String IP-Address. (Excluding detached clients)
	 * @param ipAddress which is matched against all L2PcInstances.
	 * @return List<L2PcInstance> with matching players or EMPTY list. NEVER null.
	 * @author Eli
	 */
	public static List<L2PcInstance> getByIp(String ipAddress)
	{
		return filterOnlineCharacters(e -> compareIp(ipAddress, e));
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// getByIp Methods for PLAYERS (no GMs).
	//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Returns a list of L2PcInstances without GM rights which are connected from the same IP as the given L2PcInstance. (Excluding detached clients)
	 * @param character of which IP is matched against all L2PcInstances.
	 * @return List<L2PcInstance> with matching players or EMPTY list. Never null.
	 * @author Eli
	 */
	public static List<L2PcInstance> getPlayersByIp(L2PcInstance character)
	{
		L2GameClient client = Objects.requireNonNull(character).getClient();
		if ((client == null))
		{
			return new ArrayList<>();
		}
		return getPlayersByIp(client);
	}
	
	/**
	 * Returns a list of L2PcInstances without GM rights which are connected from the same IP as the given L2GameClient. (Excluding detached clients)
	 * @param client of which IP is matched against all L2PcInstances.
	 * @return List<L2PcInstance> with matching players or EMPTY list. NEVER NULL.
	 * @author Eli
	 */
	public static List<L2PcInstance> getPlayersByIp(L2GameClient client)
	{
		String ip = Objects.requireNonNull(client).getConnectionAddress().getHostAddress();
		return getPlayersByIp(ip);
	}
	
	/**
	 * Returns a list of L2PcInstances without GM rights which are connected from the same IP as the given InetAddress. (Excluding detached clients)
	 * @param ipAddress which is matched against all L2PcInstances.
	 * @return List<L2PcInstance> with matching players or EMPTY list. NEVER NULL.
	 * @author Eli
	 */
	public static List<L2PcInstance> getPlayersByIp(InetAddress ipAddress)
	{
		String ip = Objects.requireNonNull(ipAddress).getHostAddress();
		return getPlayersByIp(ip);
	}
	
	/**
	 * Returns a list of L2PcInstances without GM rights which are connected from the same IP as the given String IP-Address. (Excluding detached clients)
	 * @param ipAddress which is matched against all L2PcInstances.
	 * @return List<L2PcInstance> with matching players or EMPTY list. NEVER null.
	 * @author Eli
	 */
	public static List<L2PcInstance> getPlayersByIp(String ipAddress)
	{
		return filterOnlineCharacters(e -> !e.isGM() && compareIp(ipAddress, e));
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// getByIp Methods for GMs (no players).
	//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Returns a list of L2PcInstances with GM rights which are connected from the same IP as the given L2PcInstance. (Excluding detached clients)
	 * @param character of which IP is matched against all L2PcInstances.
	 * @return List<L2PcInstance> with matching players or EMPTY list. Never null.
	 * @author Eli
	 */
	public static List<L2PcInstance> getGMsByIp(L2PcInstance character)
	{
		L2GameClient client = Objects.requireNonNull(character).getClient();
		if ((client == null))
		{
			return new ArrayList<>();
		}
		return getGMsByIp(client);
	}
	
	/**
	 * Returns a list of L2PcInstances with GM rights which are connected from the same IP as the given L2GameClient. (Excluding detached clients)
	 * @param client of which IP is matched against all L2PcInstances.
	 * @return List<L2PcInstance> with matching players or EMPTY list. NEVER NULL.
	 * @author Eli
	 */
	public static List<L2PcInstance> getGMsByIp(L2GameClient client)
	{
		String ip = Objects.requireNonNull(client).getConnectionAddress().getHostAddress();
		return getGMsByIp(ip);
	}
	
	/**
	 * Returns a list of L2PcInstances with GM rights which are connected from the same IP as the given InetAddress. (Excluding detached clients)
	 * @param ipAddress which is matched against all L2PcInstances.
	 * @return List<L2PcInstance> with matching players or EMPTY list. NEVER NULL.
	 * @author Eli
	 */
	public static List<L2PcInstance> getGMsByIp(InetAddress ipAddress)
	{
		String ip = Objects.requireNonNull(ipAddress).getHostAddress();
		return getGMsByIp(ip);
	}
	
	/**
	 * Returns a list of L2PcInstances with GM rights which are connected from the same IP as the given String IP-Address. (Excluding detached clients)
	 * @param ipAddress which is matched against all L2PcInstances.
	 * @return List<L2PcInstance> with matching players or EMPTY list. NEVER null.
	 * @author Eli
	 */
	public static List<L2PcInstance> getGMsByIp(String ipAddress)
	{
		return filterOnlineCharacters(e -> e.isGM() && compareIp(ipAddress, e));
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Dualbox Methods.
	//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * Find Ips/Chars with more than <Threshold> connected player.
	 * @param threshold Ips with equal or more connected chars are returned.
	 * @return Empty Map or found Players sorted by IP in a map.
	 */
	public static Map<String, List<L2PcInstance>> findDualboxByIp(int threshold)
	{
		Map<String, List<L2PcInstance>> result = new HashMap<>();
		
		for (L2PcInstance player : filterOnlineCharacters(e -> !e.isGM()))
		{
			String ip = player.getClient().getConnectionAddress().getHostAddress();
			result.computeIfAbsent(ip, k -> new ArrayList<>()).add(player);
		}
		
		// @formatter:off
		return result.entrySet().stream()
			.filter(e -> e.getValue().size() >= threshold)
			.collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));
		// @formatter:on
	}
	
	/**
	 * Find Ips/Chars with more than <Threshold> connected Chars AND with more than pcThreshold connected chars from a single pc.
	 * @param ipThreshold Ips with equal or more connected chars.
	 * @param pcThreshold PCs with equal or more connected chars.
	 * @return Empty Map or found Players sorted by IP and PCsIp in a map.
	 */
	public static Map<String, HashMap<String, List<L2PcInstance>>> findDualboxByIpAndPcIp(int ipThreshold, int pcThreshold)
	{
		Map<String, HashMap<String, List<L2PcInstance>>> result = new HashMap<>();
		
		for (Entry<String, List<L2PcInstance>> entry : findDualboxByIp(ipThreshold).entrySet())
		{
			for (L2PcInstance player : entry.getValue())
			{
				String pcIp = getPlayerTracertString(player);
				result.computeIfAbsent(entry.getKey(), k -> new HashMap<>()).computeIfAbsent(pcIp, k -> new ArrayList<>()).add(player);
			}
		}
		
		// @formatter:off
		return result.entrySet().stream()
			.filter(e -> getCountOfInnerListsMatchingSizeCriteria(e.getValue(), pcThreshold) > 0)
			.collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));
		// @formatter:on
	}
	
	private static <T, E> int getCountOfInnerListsMatchingSizeCriteria(HashMap<T, List<E>> map, int threshold)
	{
		return map.values().stream().filter(p -> p.size() >= threshold).collect(Collectors.toList()).size();
	}
}
