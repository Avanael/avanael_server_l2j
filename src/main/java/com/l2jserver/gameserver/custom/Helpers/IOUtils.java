/*
 * Copyright (C) 2016 Eli
 * 
 * This file is part of L2Next Project. (https://bitbucket.org/l2next/)
 * 
 * You are not allowed to use this class outside of L2Next Project. 
 * This includes but is not limited to distributing, modifying or use of binary 
 * or source code, complete or partial, without written permission by author.
 *
 */
package com.l2jserver.gameserver.custom.Helpers;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Eli
 */
public class IOUtils
{
	public static List<String> readFile(String pathFile)
	{
		try (FileReader fr = new FileReader(pathFile);
			BufferedReader br = new BufferedReader(fr);)
		{
			return br.lines().collect(Collectors.toList());
		}
		catch (Exception e)
		{
			return null;
		}
	}
	
	public static List<String> fetchWebUrlAsList(String url)
	{
		try
		{
			return fetchWebUrlAsList(new URL(url));
		}
		catch (MalformedURLException e)
		{
			return null;
		}
	}
	
	public static List<String> fetchWebUrlAsList(URL url)
	{
		try
		{
			try (InputStreamReader in = new InputStreamReader(url.openConnection().getInputStream());
				BufferedReader br = new BufferedReader(in))
			{
				return br.lines().collect(Collectors.toList());
			}
		}
		catch (Exception e)
		{
			return null;
		}
	}
}
