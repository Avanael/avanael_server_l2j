/*
 * Copyright (C) 2016 Eli
 * 
 * This file is part of L2Next Project. (https://bitbucket.org/l2next/)
 * 
 * You are not allowed to use this class outside of L2Next Project. 
 * This includes but is not limited to distributing, modifying or use of binary 
 * or source code, complete or partial, without written permission by author.
 *
 */
package com.l2jserver.gameserver.custom.Helpers;

import com.l2jserver.gameserver.LoginServerThread;
import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.instancemanager.PunishmentManager;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.punishment.PunishmentAffect;
import com.l2jserver.gameserver.model.punishment.PunishmentType;
import com.l2jserver.gameserver.model.variables.AccountVariables;
import com.l2jserver.gameserver.network.L2GameClient;
import com.l2jserver.gameserver.network.serverpackets.ActionFailed;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;

/**
 * @author Eli
 */
public class MiscUtils
{
	/*
	 * Get Account Variables.
	 */
	public static AccountVariables getAccountVariables(String account)
	{
		AccountVariables vars;
		L2GameClient client = LoginServerThread.getInstance().getClient(account);
		
		if ((client == null) || (client.getActiveChar() == null))
		{
			vars = new AccountVariables(account);
		}
		else
		{
			vars = client.getActiveChar().getAccountVariables();
		}
		
		return vars;
	}
	
	/**
	 * Checks if Character is affected by a Character/Account/IP-Ban of the specified PunishmentType.
	 * @param character which is checked.
	 * @param type of the Punishment.
	 * @return True if banned, false otherwise.
	 * @author Eli
	 */
	public static boolean isPunished(L2PcInstance character, PunishmentType type)
	{
		return PunishmentManager.getInstance().hasPunishment(character.getObjectId(), PunishmentAffect.CHARACTER, type) || PunishmentManager.getInstance().hasPunishment(character.getAccountName(), PunishmentAffect.ACCOUNT, type)
			|| PunishmentManager.getInstance().hasPunishment(character.getIPAddress(), PunishmentAffect.IP, type);
	}
	
	public static void sendHtml(L2PcInstance player, String htmlOrPath)
	{
		sendHtml(player, htmlOrPath, null);
	}
	
	public static void sendHtml(L2PcInstance player, String htmlOrPath, L2Npc npc)
	{
		if ((htmlOrPath == null) || htmlOrPath.isEmpty() || (player == null))
		{
			return;
		}
		
		if (htmlOrPath.endsWith(".htm") || htmlOrPath.endsWith(".html"))
		{
			// Create handler to file linked to the quest
			htmlOrPath = HtmCache.getInstance().getHtm(player.getHtmlPrefix(), htmlOrPath);
			if (htmlOrPath == null)
			{
				return;
			}
		}
		else if (!htmlOrPath.startsWith("<html"))
		{
			return;
		}
		
		final NpcHtmlMessage npcReply = new NpcHtmlMessage(npc != null ? npc.getObjectId() : 0, htmlOrPath);
		npcReply.replace("%objectId%", npc != null ? String.valueOf(npc.getObjectId()) : "0");
		npcReply.replace("%playername%", player.getName());
		player.sendPacket(npcReply);
		player.sendPacket(ActionFailed.STATIC_PACKET);
	}
}
