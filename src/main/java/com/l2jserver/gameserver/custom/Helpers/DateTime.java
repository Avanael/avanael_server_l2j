/*
 * Copyright (C) 2016 Eli
 * 
 * This file is part of L2Next Project. (https://bitbucket.org/l2next/)
 * 
 * You are not allowed to use this class outside of L2Next Project. 
 * This includes but is not limited to distributing, modifying or use of binary 
 * or source code, complete or partial, without written permission by author.
 *
 */
package com.l2jserver.gameserver.custom.Helpers;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Eli
 */
public class DateTime
{
	public static String getDate()
	{
		return new SimpleDateFormat("dd-MM-yyyy").format(new Date());
	}
	
	public static String getTime()
	{
		return new SimpleDateFormat("HH:mm:ss").format(new Date());
	}
}
