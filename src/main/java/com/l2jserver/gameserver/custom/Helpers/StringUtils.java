/*
 * Copyright (C) 2016 Eli
 * 
 * This file is part of L2Next Project. (https://bitbucket.org/l2next/)
 * 
 * You are not allowed to use this class outside of L2Next Project. 
 * This includes but is not limited to distributing, modifying or use of binary 
 * or source code, complete or partial, without written permission by author.
 *
 */
package com.l2jserver.gameserver.custom.Helpers;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * @author Eli
 */
public class StringUtils
{
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Miscellaneous String Helpers:
	//
	// repeat(String text, int times)
	// findPosistionOfAlpha(String text)
	// cutOff(String text, int length, String postfix)
	// safeString(String str)
	// safeString(String str, String defaultValue)
	// trimStringArray(String[] array)
	// generateDynamicSqlStatement(String sql, int count)
	// joinIterableToString(Iterable<T> iter, Function<T, String> function, String seperator)
	//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static String repeat(String text, int times)
	{
		String output = "";
		for (int i = 0; i < times; i++)
		{
			output += text;
		}
		return output;
	}
	
	public static int findPosistionOfAlpha(String text)
	{
		int i = 0;
		for (char c : text.toCharArray())
		{
			if (Character.isLetter(c))
			{
				return i;
			}
			i++;
		}
		return -1;
	}
	
	public static String cutOff(String text, int length, String postfix)
	{
		if (text.length() < length)
		{
			return text;
		}
		
		return text.substring(0, length).trim() + postfix;
	}
	
	public static String safeString(String text)
	{
		return safeString(text, "NULL");
	}
	
	public static String safeString(String text, String defaultValue)
	{
		if (text == null)
		{
			return defaultValue;
		}
		return text;
	}
	
	public static String[] trimStringArray(String[] array)
	{
		for (int i = 0; i < array.length; i++)
		{
			array[i] = array[i].trim();
		}
		
		return array;
	}
	
	public static String generateDynamicSqlStatement(String sql, int count)
	{
		String placeholder = "";
		for (int i = 0; i < count; i++)
		{
			placeholder += "?,";
		}
		placeholder = placeholder.substring(0, placeholder.length() - 1);
		return sql.replaceAll("%DYNAMIC%", placeholder);
	}
	
	/**
	 * <p>
	 * <b>Example with L2PcInstance Class:</b><br>
	 * sortAndJoinIterableToString(list, e -> e.getName(), ", ");
	 * </p>
	 * @param <T>
	 * @param iterable Any data source that is iterable. (Collections, Lists, Queues, Sets)
	 * @param mapFunction A mapping function.
	 * @param seperator String to join collection at the end.
	 * @return
	 */
	public static <T> String joinIterableToString(Iterable<T> iterable, Function<T, String> mapFunction, String seperator)
	{
		return StreamSupport.stream(iterable.spliterator(), false).map(mapFunction).collect(Collectors.joining(seperator));
	}
	
	/**
	 * <p>
	 * <b>Example with L2PcInstance Class:</b><br>
	 * sortAndJoinIterableToString(list, (f1, f2) -> f1.getName().compareTo(f2.getName()), e -> e.getName(), ", ");
	 * </p>
	 * @param <T>
	 * @param iterable Any data source that is iterable. (Collections, Lists, Queues, Sets)
	 * @param sortComparator A comparator to sort before mapping.
	 * @param mapFunction A mapping function.
	 * @param seperator String to join collection at the end.
	 * @return
	 */
	public static <T> String sortAndJoinIterableToString(Iterable<T> iterable, Comparator<T> sortComparator, Function<T, String> mapFunction, String seperator)
	{
		return StreamSupport.stream(iterable.spliterator(), false).sorted(sortComparator).map(mapFunction).collect(Collectors.joining(seperator));
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Methods to check string content:
	//
	// isAlpha(String content)
	// isNumeric(String content)
	// isAlphaNumeric(String content)
	//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static boolean isAlpha(String content)
	{
		if ((content == null) || content.isEmpty())
		{
			return false;
		}
		for (char c : content.toCharArray())
		{
			if (!Character.isLetter(c))
			{
				return false;
			}
		}
		return true;
	}
	
	public static boolean isNumeric(String content)
	{
		if ((content == null) || content.isEmpty())
		{
			return false;
		}
		for (char c : content.toCharArray())
		{
			if (!Character.isDigit(c))
			{
				return false;
			}
		}
		return true;
	}
	
	public static boolean isAlphaNumeric(String content)
	{
		if ((content == null) || content.isEmpty())
		{
			return false;
		}
		for (char c : content.toCharArray())
		{
			if (!Character.isLetterOrDigit(c))
			{
				return false;
			}
		}
		return true;
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Formating String Methods:
	//
	// formatAmount(double amount)
	// formatChance(double chance)
	//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static String formatAmount(double amount)
	{
		String k = "";
		double res = amount;
		while (true)
		{
			if (res < 1000)
			{
				if ((Math.round(res) == res) || (res >= 10))
				{
					return String.format("%.0f", res) + k;
				}
				return String.format("%.2f", res) + k;
			}
			res /= 1000;
			k += "k";
		}
	}
	
	public static String formatChance(double chance)
	{
		if (chance < 1)
		{
			return String.format("%.3f", chance);
		}
		if (chance < 10)
		{
			return String.format("%.2f", chance);
		}
		else if (chance < 100)
		{
			return String.format("%.1f", chance);
		}
		return String.format("%.0f", chance);
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Converting Strings to Primitive Data Types:
	//
	// String2Int(String inputString)
	// String2Int(String inputString, int defaultValue)
	// String2Double(String inputString)
	// String2Double(String inputString, double defaultValue)
	//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static Integer String2Int(String inputString)
	{
		try
		{
			return Integer.parseInt(inputString);
		}
		catch (Exception e)
		{
			return null;
		}
	}
	
	public static int String2Int(String inputString, int defaultValue)
	{
		try
		{
			return Integer.parseInt(inputString);
		}
		catch (Exception e)
		{
			return defaultValue;
		}
	}
	
	public static Double String2Double(String inputString)
	{
		try
		{
			return Double.parseDouble(inputString);
		}
		catch (Exception e)
		{
			return null;
		}
	}
	
	public static double String2Double(String inputString, double defaultValue)
	{
		try
		{
			return Double.parseDouble(inputString);
		}
		catch (Exception e)
		{
			return defaultValue;
		}
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Parsing Strings with Lists to an ARRAY OR LIST
	//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * This Method parses a list to an int[] array. Input is trim()med before every step.
	 * @author Eli
	 * @param list String to parse.
	 * @param delimiterList Delimiter of list. (Used for split)
	 * @return Parsed int[] array OR an empty array if error while parsing.
	 */
	public static int[] parseStringToIntegerArray(String list, String delimiterList)
	{
		try
		{
			String[] splittedList = list.trim().split(delimiterList);
			int[] returnArray = new int[splittedList.length];
			
			for (int i = 0; i < splittedList.length; i++)
			{
				returnArray[i] = Integer.parseInt(splittedList[i].trim());
			}
			
			return returnArray;
		}
		catch (Exception e)
		{
			return new int[0]; // Something wrong!
		}
	}
	
	/**
	 * This Method parses a list to a List<Integer>. Input is trim()med before every step.
	 * @author Eli
	 * @param list String to parse.
	 * @param delimiterList Delimiter of list. (Used for split)
	 * @return Parsed List<Integer> array OR a empty list if error while parsing.
	 */
	public static List<Integer> parseStringToIntegerList(String list, String delimiterList)
	{
		try
		{
			String[] splittedList = list.trim().split(delimiterList);
			List<Integer> returnList = new ArrayList<>(splittedList.length);
			
			for (String element : splittedList)
			{
				returnList.add(Integer.parseInt(element.trim()));
			}
			
			return returnList;
		}
		catch (Exception e)
		{
			return new ArrayList<>(); // Something wrong!
		}
	}
	
	/**
	 * This Method parses a list to an Double[] array. Input is trim()med before every step.
	 * @author Eli
	 * @param list String to parse.
	 * @param delimiterList Delimiter of list. (Used for split)
	 * @return Parsed Double[] array OR an empty array if error while parsing.
	 */
	public static double[] parseStringToDoubleArray(String list, String delimiterList)
	{
		try
		{
			String[] splittedList = list.trim().split(delimiterList);
			double[] returnArray = new double[splittedList.length];
			
			for (int i = 0; i < splittedList.length; i++)
			{
				returnArray[i] = Double.parseDouble(splittedList[i].trim());
			}
			
			return returnArray;
		}
		catch (Exception e)
		{
			return new double[0]; // Something wrong!
		}
	}
	
	/**
	 * This Method parses a list to a List<Double>. Input is trim()med before every step.
	 * @author Eli
	 * @param list String to parse.
	 * @param delimiterList Delimiter of list. (Used for split)
	 * @return Parsed List<Double> OR a empty list if error while parsing.
	 */
	public static List<Double> parseStringToDoubleList(String list, String delimiterList)
	{
		try
		{
			String[] splittedList = list.trim().split(delimiterList);
			List<Double> returnList = new ArrayList<>(splittedList.length);
			
			for (String element : splittedList)
			{
				returnList.add(Double.parseDouble(element.trim()));
			}
			
			return returnList;
		}
		catch (Exception e)
		{
			return new ArrayList<>(); // Something wrong!
		}
	}
	
	/**
	 * This Method parses a list to an String[] array. Input is trim()med before every step.
	 * @author Eli
	 * @param list String to parse.
	 * @param delimiterList Delimiter of list. (Used for split)
	 * @return Parsed String[] array OR an empty array if error while parsing.
	 */
	public static String[] parseStringToStringArray(String list, String delimiterList)
	{
		try
		{
			String[] splittedList = list.trim().split(delimiterList);
			String[] returnArray = new String[splittedList.length];
			
			for (int i = 0; i < splittedList.length; i++)
			{
				returnArray[i] = splittedList[i].trim();
			}
			
			return returnArray;
		}
		catch (Exception e)
		{
			return new String[0]; // Something wrong!
		}
	}
	
	/**
	 * This Method parses a list to a List<String>. Input is trim()med before every step.
	 * @author Eli
	 * @param list String to parse.
	 * @param delimiterList Delimiter of list. (Used for split)
	 * @return Parsed List<String> OR a empty list if error while parsing.
	 */
	public static List<String> parseStringToStringString(String list, String delimiterList)
	{
		try
		{
			String[] splittedList = list.trim().split(delimiterList);
			List<String> returnList = new ArrayList<>(splittedList.length);
			
			for (String element : splittedList)
			{
				returnList.add(element.trim());
			}
			
			return returnList;
		}
		catch (Exception e)
		{
			return new ArrayList<>(); // Something wrong!
		}
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Parsing Strings with List and Sub-Lists to an ARRAY
	//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * This Method parses a list with a sublist to a int[][] array. Input is trim()med before every step.
	 * @author Eli
	 * @param list String to parse.
	 * @param delimiterList Delimiter of (outer) List. (First split)
	 * @param delimiterInnerList Delimiter of (inner) Sub-List. (Second split)
	 * @param expectedSizeInnerList Expected size of inner list. (Array-Size of inner/second split. NOT character count)
	 * @return Parsed int[][] array OR an empty array if error while parsing.
	 */
	public static int[][] parseStringWithSublistToIntegerArray(String list, String delimiterList, String delimiterInnerList, int expectedSizeInnerList)
	{
		try
		{
			String[] splittedList = list.trim().split(delimiterList);
			int[][] returnArray = new int[splittedList.length][expectedSizeInnerList];
			
			for (int i = 0; i < splittedList.length; i++)
			{
				String[] innerSplit = splittedList[i].trim().split(delimiterInnerList);
				
				if (innerSplit.length != expectedSizeInnerList)
				{
					return new int[0][0]; // Something wrong!
				}
				
				for (int j = 0; j < expectedSizeInnerList; j++)
				{
					returnArray[i][j] = Integer.parseInt(innerSplit[j].trim());
				}
			}
			return returnArray;
		}
		catch (Exception e)
		{
			return new int[0][0]; // Something wrong!
		}
	}
	
	/**
	 * This Method parses a list with a sublist to a Double[][] array. Input is trim()med before every step.
	 * @author Eli
	 * @param list String to parse.
	 * @param delimiterList Delimiter of (outer) List. (First split)
	 * @param delimiterInnerList Delimiter of (inner) Sub-List. (Second split)
	 * @param expectedSizeInnerList Expected size of inner list. (Array-Size of inner/second split. NOT character count)
	 * @return Parsed Double[][] array OR an empty array if error while parsing.
	 */
	public static double[][] parseStringWithSublistToDoubleArray(String list, String delimiterList, String delimiterInnerList, int expectedSizeInnerList)
	{
		try
		{
			String[] splittedList = list.trim().split(delimiterList);
			double[][] returnArray = new double[splittedList.length][expectedSizeInnerList];
			
			for (int i = 0; i < splittedList.length; i++)
			{
				String[] innerSplit = splittedList[i].trim().split(delimiterInnerList);
				
				if (innerSplit.length != expectedSizeInnerList)
				{
					return new double[0][0]; // Something wrong!
				}
				
				for (int j = 0; j < expectedSizeInnerList; j++)
				{
					returnArray[i][j] = Double.parseDouble(innerSplit[j].trim());
				}
			}
			return returnArray;
		}
		catch (Exception e)
		{
			return new double[0][0]; // Something wrong!
		}
	}
	
	/**
	 * This Method parses a list with a sublist to a String[][] array. Input is trim()med before every step.
	 * @author Eli
	 * @param list String to parse.
	 * @param delimiterList Delimiter of (outer) List. (First split)
	 * @param delimiterInnerList Delimiter of (inner) Sub-List. (Second split)
	 * @param expectedSizeInnerList Expected size of inner list. (Array-Size of inner/second split. NOT character count)
	 * @return Parsed String[][] array OR an empty array if error while parsing.
	 */
	public static String[][] parseStringWithSublistToStringArray(String list, String delimiterList, String delimiterInnerList, int expectedSizeInnerList)
	{
		try
		{
			String[] splittedList = list.trim().split(delimiterList);
			String[][] returnArray = new String[splittedList.length][expectedSizeInnerList];
			
			for (int i = 0; i < splittedList.length; i++)
			{
				String[] innerSplit = splittedList[i].trim().split(delimiterInnerList);
				
				if (innerSplit.length != expectedSizeInnerList)
				{
					return new String[0][0]; // Something wrong!
				}
				
				for (int j = 0; j < expectedSizeInnerList; j++)
				{
					returnArray[i][j] = innerSplit[j].trim();
				}
			}
			return returnArray;
		}
		catch (Exception e)
		{
			return new String[0][0]; // Something wrong!
		}
	}
}
