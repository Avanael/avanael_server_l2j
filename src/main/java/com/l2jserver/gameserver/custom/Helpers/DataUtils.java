/*
 * Copyright (C) 2016 Eli
 * 
 * This file is part of L2Next Project. (https://bitbucket.org/l2next/)
 * 
 * You are not allowed to use this class outside of L2Next Project. 
 * This includes but is not limited to distributing, modifying or use of binary 
 * or source code, complete or partial, without written permission by author.
 *
 */
package com.l2jserver.gameserver.custom.Helpers;

/**
 * @author Eli
 */
public class DataUtils
{
	public static int integerSafeDecode(String text, int defaultValue)
	{
		try
		{
			return Integer.decode(text.trim());
		}
		catch (NumberFormatException e)
		{
			return defaultValue;
		}
	}
}
